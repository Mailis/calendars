import { Calendar, momentLocalizer } from 'react-big-calendar'
import moment from 'moment'
import {getAllEvents} from '../controllers/evetsController';

import '../less/styles.less';
import '../sass/styles.scss';

// Setup the localizer by providing the moment (or globalize) Object
// to the correct localizer.
const localizer = momentLocalizer(moment);
const myEventsList = getAllEvents(); // or globalizeLocalizer
// console.log("localizer", localizer);
const MyCalendar = props => (
    
  <div className="mycalendar">
    <Calendar

popup
popupOffset={{ x: -10, y: -20 }}
      localizer={localizer}
      events={myEventsList}
      startAccessor="start"
      endAccessor="end"
    />
  </div>
);
export default MyCalendar;